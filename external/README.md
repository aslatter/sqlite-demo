This folder contains dependencies of this project.

Each sub-folder is a separate work, with separate authorship and
licensure, distinct from the main project. Please review the source,
documentation, or other resources per sub-folder for details.
