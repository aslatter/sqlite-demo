
CONFIG_FLAGS :=

HAS_NINJA := $(shell command -v ninja 2> /dev/null)
ifdef HAS_NINJA
CONFIG_FLAGS += -GNinja
endif

all: configure build
.PHONY: all

configure:
	cmake . -Bbuild $(CONFIG_FLAGS) -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
.PHONY: configure

build:
	cmake --build build
.PHONY: build

clean:
	rm -rf build
.PHONY: build

tidy:
	@ clang-tidy -fix-errors -checks='-*,bugprone-*,cppcoreguidelines-*,-cppcoreguidelines-pro-bounds-pointer-arithmetic,-cppcoreguidelines-pro-type-vararg,-cppcoreguidelines-pro-bounds-array-to-pointer-decay,-cppcoreguidelines-avoid-magic-numbers,google-runtime-references,google-explicit-constructor,misc-*,modernize-*,-modernize-use-trailing-return-type,-modernize-use-nodiscard,performance-*,readability-*,-readability-magic-numbers' -p build $(shell find src -type f -name '*.c')
.PHONY: tidy

format:
	@ clang-format -i $(shell find src -type f -name '*.c')
.PHONY: format

