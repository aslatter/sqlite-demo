#include "migrations.h"

#include "sqlite3.h"

#include <stdbool.h>
#include <stdio.h>

// local forward declarations

static int get_user_version(sqlite3 *db, int *version);
static int set_user_version(sqlite3 *db, int new_version);
static int migrate_db_from(sqlite3 *db, int from_version);

// constants

#define desired_version 1 // bump this to add a new migration

// logic

static int migrate_db_from(sqlite3 *db, int from_version) {
  char *cmd = "";
  int rc = 0;
  switch (from_version) {
  case 0:
    cmd = "\
    -- enable WAL\n\
    PRAGMA journal_mode=WAL;\n\
    \n\
    -- create base table\n\
    create table db (\n\
      key text primary key,\n\
      value text not null\n\
    ) without rowid;\n\
    ";

    // run script
    rc = sqlite3_exec(db, cmd, NULL, NULL, NULL);
    if (rc != SQLITE_OK) {
      return rc;
    }

    // use fall-through to next version to add a new migration
    // (or we could define a constant array of sql-scripts?)
  }

  return SQLITE_OK;
}

bool migrate_db(sqlite3 *db) {
  int version = 0;
  int rc = get_user_version(db, &version);
  if (rc != SQLITE_OK) {
    fprintf(stderr, "unable to get user version: %s\n", sqlite3_errstr(rc));
    return false;
  }

  if (version >= desired_version) {
    return true;
  }

  // open a tx to migrate
  rc = sqlite3_exec(db, "BEGIN EXCLUSIVE TRANSACTION;", NULL, NULL, NULL);
  if (rc != SQLITE_OK) {
    fprintf(stderr, "unable to begin migration tx: %s\n", sqlite3_errstr(rc));
    return false;
  }

  version = 0;
  rc = get_user_version(db, &version);
  if (rc != SQLITE_OK) {
    // ??
    fprintf(stderr, "unable to get user version: %s\n", sqlite3_errstr(rc));
    sqlite3_exec(db, "ROLLBACK TRANSACTION;", NULL, NULL, NULL);
    return false;
  }

  if (version >= desired_version) {
    // version is actually okay, no need to migrate
    // TODO - print error if failed?
    sqlite3_exec(db, "ROLLBACK TRANSACTION;", NULL, NULL, NULL);
    return true;
  }

  // do actual migrations
  rc = migrate_db_from(db, version);
  if (rc != SQLITE_OK) {
    fprintf(stderr, "error performing migrations: %s\n", sqlite3_errstr(rc));
    sqlite3_exec(db, "ROLLBACK TRANSACTION;", NULL, NULL, NULL);
    return false;
  }

  // done with migrations, write-back new version
  rc = set_user_version(db, desired_version);
  if (rc != SQLITE_OK) {
    // abort tx & fail
    fprintf(stderr, "unable to update user version: %s\n", sqlite3_errstr(rc));
    sqlite3_exec(db, "ROLLBACK TRANSACTION;", NULL, NULL, NULL);
    return false;
  }

  // fin!
  rc = sqlite3_exec(db, "COMMIT TRANSACTION;", NULL, NULL, NULL);
  if (rc != SQLITE_OK) {
    fprintf(stderr, "unable to commit db migration: %s\n", sqlite3_errstr(rc));
    return false;
  }
  return true;
}

static int get_user_version(sqlite3 *db, int *version) {
  // prepare statement
  sqlite3_stmt *stmt = NULL;
  int rc = sqlite3_prepare_v2(db, "PRAGMA user_version;", -1, &stmt, NULL);
  if (rc != SQLITE_OK) {
    return rc;
  }

  // step
  rc = sqlite3_step(stmt);
  if (rc != SQLITE_ROW) {
    // uh-oh
    sqlite3_finalize(stmt);
    return rc;
  }

  *version = sqlite3_column_int(stmt, 0);

  // fin
  sqlite3_finalize(stmt);

  return SQLITE_OK;
}

static int set_user_version(sqlite3 *db, int new_version) {
  // sqlite doesn't support "binding" to a pragma-assignment rhs
  const int len = 100;
  char stmt[len];
  int ret = snprintf(stmt, len, "PRAGMA user_version = %d;", new_version);
  if (ret >= len) {
    // shouldn't happen
    return SQLITE_ERROR;
  }
  return sqlite3_exec(db, stmt, NULL, NULL, NULL);
}