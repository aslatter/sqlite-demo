#pragma once

#include "sqlite3.h"

#include <stdbool.h>

bool migrate_db(sqlite3 *db);
