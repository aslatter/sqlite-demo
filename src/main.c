#include "migrations.h"

#include "sqlite3.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct {
  char *db_file;
} opts;

bool parse_args(int argc, char **argv);
void print_usage_and_exit(int code);

int main(int argc, char **argv) {
  if (!parse_args(argc, argv)) {
    print_usage_and_exit(EXIT_FAILURE);
  }

  sqlite3 *db = NULL;
  int rc = 0;

  rc = sqlite3_open(opts.db_file, &db);
  if (rc) {
    fprintf(stderr, "Can't open database: %s\n", opts.db_file);
    sqlite3_close(db);
    return EXIT_FAILURE;
  }

  if (!migrate_db(db)) {
    puts("error performing db migrations!");
    sqlite3_close(db);
    return EXIT_FAILURE;
  }

  sqlite3_close(db);
}

bool parse_args(int argc, char **argv) {
  if (argc < 2) {
    return false;
  }
  opts.db_file = argv[1];

  return true;
}

void print_usage_and_exit(int code) { exit(code); }
